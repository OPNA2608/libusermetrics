find_package(GSettings)

###########################
# Settings
###########################

add_schema("com.lomiri.UserMetrics.gschema.xml")

install(
	DIRECTORY
		libusermetrics
	DESTINATION
		${CMAKE_INSTALL_FULL_DATADIR}
)

###########################
# Policy
###########################

set(
	DBUSCONFDIR
	"${CMAKE_INSTALL_FULL_SYSCONFDIR}/dbus-1/system.d"
)

install(
	FILES
	"com.lomiri.UserMetrics.conf"
	DESTINATION ${DBUSCONFDIR}
)

###########################
# Dbus Interfaces
###########################

set(
	DBUSIFACEDIR
	"${CMAKE_INSTALL_FULL_DATADIR}/dbus-1/interfaces/"
)

install(
	FILES
	com.lomiri.usermetrics.DataSet.xml
	com.lomiri.usermetrics.DataSource.xml
	com.lomiri.usermetrics.UserData.xml
	com.lomiri.UserMetrics.xml
	DESTINATION ${DBUSIFACEDIR}
)

###########################
# Dbus Services
###########################

set(
	DBUSSERVICEDIR
	"${CMAKE_INSTALL_FULL_DATADIR}/dbus-1/system-services/"
)

set(USERMETRICS_SERVICE
	"${CMAKE_CURRENT_BINARY_DIR}/com.lomiri.UserMetrics.service"
)

configure_file(
	"com.lomiri.UserMetrics.service.in"
	${USERMETRICS_SERVICE}
	@ONLY
)

install(
	FILES
	${USERMETRICS_SERVICE}
	DESTINATION ${DBUSSERVICEDIR}
)
